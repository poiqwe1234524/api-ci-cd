require('dotenv').config()
const supertest = require('supertest');

const api = supertest(process.env.BASE_URL)

const favorit = (token, slug) => api.post(`/api/articles/${slug}/favorite`)
    .set('Authorization', `Token ${token}`)

module.exports = {
    favorit
}