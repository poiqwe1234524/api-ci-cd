const { expect } = require('chai')
const { getToken } = require('../page/get-token-page')
const keysUser = ['id', 'email', 'bio', 'createdAt', 'image', 'token', 'updatedAt', 'username']
const { dataToken } = require('../data/get-token-data')

const testCase = {
    postiive: {
        success: 'sebagai saya user saya ingin mendapatkan token'
    }
}

describe('Token', () => {
    let response
    it(testCase.postiive.success, async () => {
        response = await getToken(dataToken);
        expect(response.status).to.equal(200)
        expect(response.body).to.have.all.keys(['user'])
        expect(response.body.user).to.have.all.keys(keysUser)
    })

    it('sebagai saya tidak akan mendapatkan token tanpa request body', async () => {
        response = await getToken();
        expect(response.status).to.equal(422);
        expect(response.body).to.have.all.keys(['errors'])
        expect(response.body.errors).to.have.all.keys(['email or password'])
    })
})