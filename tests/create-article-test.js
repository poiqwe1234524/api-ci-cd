const { expect } = require('chai')
const { createArticle } = require('../page/create-article-page')
const { getToken } = require('../page/get-token-page')
const { dataToken } = require('../data/get-token-data')

const { dataArticle } = require('../data/create-article-data')

describe('Article', () => {
    let response
    it('sebagai saya user saya ingin membuat article', async () => {
        response = await getToken(dataToken);
        response = await createArticle(response.body.user.token, dataArticle)
        expect(response.status).to.equal(200)
    })


    it('sebagai saya tidak akan membuat article tanpa request body', async () => {
        const response = await getToken();
        expect(response.status).to.equal(422);
    })
})