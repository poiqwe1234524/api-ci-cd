const { expect } = require('chai')
const { createArticle } = require('../page/create-article-page')
const { getToken } = require('../page/get-token-page')
const { favorit } = require('../page/favorit-page')
const { dataToken } = require('../data/get-token-data')
const { dataArticle } = require('../data/create-article-data')

describe('favorit', () => {
    let response
    let token
    it('sebagai user saya ingin memfavoritkan article', async () => {
        response = await getToken(dataToken);
        token = response.body.user.token
        response = await createArticle(token, dataArticle)
        response = await favorit(token, response.body.article.slug)
        expect(response.status).to.equal(400)
    })


    it('sebagai saya tidak akan membuat article tanpa request body', async () => {
        const response = await getToken();
        expect(response.status).to.equal(422);
        // expect(response.body).to.have.all.keys(['errors'])
        // expect(response.body.errors).to.have.all.keys(['ail or password']) 
    })
})